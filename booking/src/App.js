import { Routes, Route } from "react-router-dom";
import Layout from "./Components/Common/Layout";
import ArrangeConversation from "./pages/ArrangeConversation";
import MusicRequests from "./pages/MusicRequests";
import CreatePlaylist from "./pages/CreatePlaylist";
import Welcome from "./pages/Welcome";
import YourAppointments from "./pages/YourAppointments";
import YourData from "./pages/YourData";
import SelectPackage from "./pages/SelectPackage";
import AdditionalProducts from "./pages/AdditionalProducts";
import Checkout from "./pages/Checkout";
import Payment from "./pages/Payment";

const App = () => {
  return (
    <Routes>
      <Route index element={<Welcome />} />
      <Route path="/" element={<Layout />}>
        <Route path="music-wish" element={<MusicRequests />} />
        <Route path="your-data" element={<YourData />} />
        <Route path="create-playlist" element={<CreatePlaylist />} />
        <Route path="arrange-conversation" element={<ArrangeConversation />} />
        <Route path="your-appointments" element={<YourAppointments />} />
        {/* new pages */}
        <Route path="select-package" element={<SelectPackage />} />
        <Route path="additional-products" element={<AdditionalProducts />} />
        <Route path="checkout" element={<Checkout />} />
        <Route path="payment" element={<Payment />} />
      </Route>
    </Routes>
  );
};

export default App;

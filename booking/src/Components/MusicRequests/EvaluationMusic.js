import React, { useState, useEffect } from 'react';
import StarRatingComponent from 'react-star-rating-component';

export default ({ onChange }) => {
    const [rating, setRating] = useState(0);

    useEffect(() => {
        if (rating === 0) return;
        onChange(rating);
    }, [rating]);

    return (
        <div>
            <StarRatingComponent
                renderStarIcon={() => <span style={{ fontSize: 24, }}> ★</span>}
                emptyStarColor="#000000"
                starColor="#FFA114"
                name="rate1"
                starCount={5}
                value={rating}
                onStarClick={(nextValue) => setRating(nextValue)}
            />
        </div>)
}
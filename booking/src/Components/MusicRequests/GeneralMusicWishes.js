import React from 'react';

function GeneralMusicWishes({ initialValue, setInitialValue }) {

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setInitialValue(values => ({ ...values, [name]: value }))
  }

  return (
    <div className="col-12 col-md-10">
      <div className="form-group">
        <label className="label" htmlFor="first-song">
          Erstes Lied
        </label>
        <input
          type="text"
          className="form-control"
          id="first-song"
          placeholder="Erstes Lied"
          name="firstSong"
          value={initialValue.firstSong}
          onChange={handleChange}
        />
      </div>
      <div className="form-group">
        <label className="label" htmlFor="sp-song">
          NO GO’s - Genres oder spezielle Lieder
        </label>
        <textarea
          className="form-control"
          id="sp-song"
          placeholder="Spezielle Lieder"
          name="specialSongs"
          value={initialValue.specialSongs}
          onChange={handleChange}
        ></textarea>
      </div>
      <div className="form-group">
        <label className="label" htmlFor="miscellaneous">
          Sonstiges
        </label>
        <textarea
          className="form-control"
          id="miscellaneous"
          placeholder="Sonstiges"
          name="miscellaneous"
          value={initialValue.miscellaneous}
          onChange={handleChange}
        ></textarea>
      </div>
    </div>
  )
} export default GeneralMusicWishes;

import React from 'react';
import EvaluationMusic from "./EvaluationMusic"
import bild1 from "../../assets/images/bild1.png";
import bild2 from "../../assets/images/bild2.png";
import bild3 from "../../assets/images/bild3.png";
import bild4 from "../../assets/images/bild4.png";
import bild5 from "../../assets/images/bild5.png";
import bild6 from "../../assets/images/bild6.png";
import bild7 from "../../assets/images/bild7.png";
import bild8 from "../../assets/images/bild8.png";
import bild9 from "../../assets/images/bild9.png";
import bild10 from "../../assets/images/bild10.png";
import bild11 from "../../assets/images/bild11.png";
import bild12 from "../../assets/images/bild12.png";
import bild13 from "../../assets/images/bild13.png";
import bild14 from "../../assets/images/bild14.png";
import bild15 from "../../assets/images/bild15.png";

const list = [
  { categorie: "80s Classics", bild: bild1},
  { categorie: "90s Classics", bild: bild2},
  { categorie: "2000er Classics", bild: bild3},
  { categorie: "10er Party", bild: bild4},
  { categorie: "Deutsch Rap Oldskool", bild: bild5},
  { categorie: "Alternative & Rock Pop", bild: bild6},
  { categorie: "House Classics", bild: bild7},
  { categorie: "Funk-,Soul-& Dance Classics", bild: bild8},
  { categorie: "Latin", bild: bild9},
  { categorie: "Hard-& New Metal Rock", bild: bild10},
  { categorie: "R&B $ Black", bild: bild11},
  { categorie: " Rap & Hip Hop", bild: bild12},
  { categorie: '"Kölsche" Musik', bild: bild13},
  { categorie: "Schlager & Skihütte", bild: bild14},
  { categorie: "Ballermann & Trash", bild: bild15},
];

const leerArray = [];
const chunkSize = 4;
for (let i = 0; i < list.length; i += chunkSize) {
  const chunk = list.slice(i, i + chunkSize);
  leerArray.push(chunk);
}

const ListMusic = ({ onChange }) => {

  return (leerArray.map(row => {
    return (
      <div class="row my-3 " >
        {row.map(item => {
          return (
            <div class="col-6 col-md-3">
              <div > <img class="rounded mb-1" style={{ height: 100, width: "100%" }} src={item.bild} /></div>
              <strong >{item.categorie}</strong>
              <EvaluationMusic onChange={(rating) => onChange(item.categorie, rating)} />
            </div>
          )
        })}
      </div>)
  })
  )
}
export default ListMusic;

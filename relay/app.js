const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const helmet = require("helmet");

const app = express();
const port = 3001;

const corsConfig = {
  origin: "*",
  optionsSuccessStatus: 200,
};

app.use(helmet());
app.use(bodyParser.json());
app.use(cors(corsConfig));
app.use("/api", require("./routes/main"));

app.listen(port, () => {});

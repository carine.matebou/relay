const axios = require("axios");
const express = require("express");
const { PIPEDRIVE_ADMIN_API_KEY, PIPEDRIVE_ADMIN_URL } = require("../constant");

const router = express.Router();
const cors = require("cors");

router.post("/organizations", cors(), async (req, res) => {
  try {
    const {
      company,
      name,
      email,
      phone,
      title,
      content,
      dateofevent,
      budget,
      guests,
      residencelocation,
      habitacion,
    } = req.body;

    const resOrganization = await axios.post(
      `${PIPEDRIVE_ADMIN_URL}/organizations?api_token=${PIPEDRIVE_ADMIN_API_KEY}`,
      {
        name: company,
        visible_to: 3,
      },
      {
        headers: {
          "Content-Type": "application/json",
        },
      }
    );

    const org_id = resOrganization.data?.data?.id;

    const resPerson = await axios.post(
      `${PIPEDRIVE_ADMIN_URL}/persons?api_token=${PIPEDRIVE_ADMIN_API_KEY}`,
      {
        name: name,
        org_id,
        email: email,
        phone: phone,
        visible_to: 3,
      }
    );
    const person_id = resPerson?.data?.data?.id;

    const resLeads = await axios.post(
      `${PIPEDRIVE_ADMIN_URL}/leads?api_token=${PIPEDRIVE_ADMIN_API_KEY}`,
      {
        title: title,
        person_id,
        organization_id: org_id,
        visible_to: "3",
        was_seen: false,
      }
    );
    const lead_id = resLeads?.data?.data?.id;

    const noteMusic = await axios.post(
      `${PIPEDRIVE_ADMIN_URL}/notes?api_token=${PIPEDRIVE_ADMIN_API_KEY}`,
      {
        lead_id,
        person_id,
        org_id,
        content,
      }
    );

    const noteData = await axios.post(
      `${PIPEDRIVE_ADMIN_URL}/notes?api_token=${PIPEDRIVE_ADMIN_API_KEY}`,
      {
        lead_id,
        person_id,
        org_id,
        content: `Eventdatum: ${dateofevent} \n Budget: ${budget} \n Gastanzahl: ${guests} \n Location: ${residencelocation} \n Wohnort: ${habitacion}`,
      }
    );

    return res.send({
      isSuccess: true,
      message: "Data saved successfully.",
      data: noteMusic?.data?.data,
    });
  } catch (error) {
    res.send({ isSuccess: false, message: error?.response?.message });
  }
});

router.post("/activity", cors(), async (req, res) => {
  try {
    const {
      event_name,
      due_date,
      org_id,
      due_time,
      lead_id,
      person_id,
      public_description,
      duration,
      note,
      email,
    } = req.body;
    const response = await axios.post(
      `${PIPEDRIVE_ADMIN_URL}/activities?api_token=${PIPEDRIVE_ADMIN_API_KEY}`,
      {
        subject: event_name,
        public_description: public_description,
        due_date,
        due_time,
        duration,
        note,
        lead_id: lead_id,
        person_id: person_id,
        org_id: org_id,
        type: "meeting",
        attendees: [{ email_address: email }],
      }
    );
    return res.send(response.data);
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
